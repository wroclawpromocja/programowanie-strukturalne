PROGRAMOWANIE
==============

C, C++
--------------

Materiały do zajęć z przedmiotu PROGRAMOWANIE STRUKTURALNE I OBIEKTOWE

IDE
--------------

**Code::Block**

Pracować będziemy z IDE dostępnym pod wszystkie platformy jakim jest [Code::Block](http://codeblocks.org/). W sieci dostępnych jest całkiem sporo różnych IDE pod C czy C++ wybrałem ten jako że jego obsługa jest intuicyjna, posiada prosty i w pełni konfigurowalny interfejs, nie jest oparty na żadnym tzw. silniku wirtualnym (np. java), nie wymaga obsługi konkretnego webbrowsera generującego GUI z kodu HTML, CSS, JS... Zatem jest dość szybki i stabilny. Wersja dostępna jest dla każdej popularnej platformy, w tym dla: Windows, Linux _(tu sprawdź repozytoria nim pobierzesz)_.

* http://codeblocks.org/

Linki / pomoce / kursy
---------------

**Mirosław ZELENT**

* http://miroslawzelent.pl/
* [Skuteczna nauka programowania](https://www.youtube.com/watch?v=nXwDEL-ivis&list=PLOYHgt8dIdoxUJ8j9AkLLFmBMBqS9LUeh)
* [C++, kurs programowania od podstaw dla każdego](https://www.youtube.com/watch?v=ErOzmh3BiXU&list=PLOYHgt8dIdoxx0Y5wzs7CFpmBzb40PaDo) :: YouTube, vlog

Lektura
---------------

Na samym początku każda książka do nauki wydaje się być właściwa. Niestety, czasem autor trafi ze swoją wiedzą do Ciebie, ale nie do mnie, zatem nie będę sugerował konkretnych tytułów. Opinie o poszczególnych tytułach dostępne są w Internecie, na stronach owych księgarni. Mamy XXI wiek, nie ma potrzeby zawierzania mi na słowo że dany tytuł jest dobry - no i... nie przeczytałem wszystkich książek... tak więc:

**HELION**

* [C, programowanie strukturalne (i niekoniecznie)](http://helion.pl/kategorie/programowanie/c)
* [C++, programowanie nie tylko **obiektowe**](http://helion.pl/kategorie/programowanie/c++)
* [UML, projektowanie aplikacji schematem blokowym](http://helion.pl/kategorie/programowanie/uml)

**PWN / WNT, Wydawnictwo Naukowo Techniczne**

* [Programowanie Strukturalna i Obiektowe (T1)](http://ksiegarnia.pwn.pl/produkt/10549/programowanie-strukturalne-i-obiektowe-t-1.html) :: książka
* [Programowanie, książki wydawnictwa WNT](http://ksiegarnia.pwn.pl/kategoria/125281,20320/wydawca/wnt/programowaniejezyki-programowania.html) :: e-księgarnia, polecany dział

Jako ciekawostki, tzw. wyższa szkoła jazdy
-----------------

* http://asawicki.info/Download/Productions/Lectures/IGK2011_Dabrowski_Sawicki_slides.pdf (język **POLSKI**) PL
* https://graphics.stanford.edu/~seander/bithacks.html
* http://www.codeproject.com/Articles/30815/An-Anti-Reverse-Engineering-Guide

**Testowanie swojego kodu** | _Bo najwyższą formą zaufania jest kontrola_


* [TDD, test driven development](https://pl.wikipedia.org/wiki/Test-driven_development)
* [GMOCK, testy jednostkowe z pomocą frameworka od Google](https://code.google.com/p/googlemock/) oraz to co tygryski lubią najbardziej [GoogleTest :: on GitHUB](https://github.com/google/googletest)
* [CppCheck, szukanie dziur i wycieków w kodzie C++](http://cppcheck.sourceforge.net/) plus trochę mniej teorii ([CppCheck on WikiPedia](https://en.wikipedia.org/wiki/Cppcheck))

Informacje dodatkowe
---------------

**>> GIT** _| kurs od a-z, język polski | **PL**_

* https://git-scm.com/book/pl/v1/Pierwsze-kroki-Podstawy-Git