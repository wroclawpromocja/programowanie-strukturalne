#include <stdio.h>
#include <stdlib.h>

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
/**
 * @brief   To jest deklaracja funkcji przykładowej
 *          nie zwracającej żadnej wartości i nie
 *          pobierającej żadnej wartości (argumentów)
 */
void funkcja_ktora_nic_nie_chce ( void );

/**
 * @brief   To jest deklaracja funkcji zwracającej
 *          wartość liczbową typu rzeczywistego
 */
int funkcja_ktora_zwraca_liczbe_calkowita ( void );

/**
 * @brief   To jest deklaracja funkcji pobierającej
 *          wartość liczbową typu rzeczywistego,
 *          nic nie zwracającej.
 */
void funkcja_ktora_pobiera_liczbe_calkowita ( int );

/**
 * @brief   To jest deklaracja funkcji pobierającej
 *          wartość liczbową typu rzeczywistego,
 *          zwracającej tę samą liczbę po dodaniu
 *          do niej dowolnej liczby.
 */
void funkcja_ktora_zwraca_zmieniona_wartosc ( int );

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
/*
 *
 */
int main()
{
    printf("!\n");
    return 0;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
/*
 *
 */
void funkcja_ktora_nic_nie_chce ( void )
{
    printf("wykonanie: funkcja_ktora_nic_nie_chce \n");
}

/*
 *
 */
int funkcja_ktora_zwraca_liczbe_calkowita ( void )
{
    printf("wykonanie: funkcja_ktora_zwraca_liczbe_calkowita \n");
    return 13;
}

/*
 *
 */
void funkcja_ktora_pobiera_liczbe_calkowita ( int x )
{
    printf("wykonanie: funkcja_ktora_pobiera_liczbe_calkowita, jest to liczba: %d \n", x);
}

/*
 *
 */
int funkcja_ktora_zwraca_zmieniona_wartosc ( int x )
{
    printf("wykonanie: funkcja_ktora_zwraca_zmieniona_wartosc, wartość pobrana: %d \n", x);
    return ( x + 13 );
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// EOF
